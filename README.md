# Boilerplate for [pixi.js](https://github.com/pixijs/pixi.js)

## Features

-   TypeScript
-   Reactive Entity Component System ([ecsy](https://github.com/MozillaReality/ecsy))
-   Input management
-   Sound integration ([pixi-sound](https://github.com/pixijs/pixi-sound))
-   Physics engine ([planck.js](https://github.com/shakiba/planck.js))
-   Viewport/Camera ([pixi-viewport](https://github.com/davidfig/pixi-viewport))
-   FPS counter ([yy-fps](https://github.com/davidfig/fps))
-   Web and Desktop (Electron) development with hot reloading and production build

## Installation

```bash
lerna bootstrap
```

## Run development

### Web

```bash
yarn run web:start
```

### Desktop

```bash
yarn run desktop:start
```

## Build distribution

### Web

```bash
yarn run web:dist
```

### Desktop

```bash
yarn run desktop:dist
```
