import { Application, Graphics, Point } from 'pixi.js';
import { Entity } from 'ecsy';
import { Box, Circle, Vec2, World } from 'planck-js';
import { ColorReplaceFilter } from '@pixi/filter-color-replace';

import EntityManager from '../ecs/EntityManager';
import PositionComponent from '../ecs/components/PositionComponent';
import VelocityComponent from '../ecs/components/VelocityComponent';
import RenderableComponent from '../ecs/components/RenderableComponent';
import SpeedComponent from '../ecs/components/SpeedComponent';
import RotationComponent from '../ecs/components/RotationComponent';
import BodyComponent from '../ecs/components/BodyComponent';
import StateComponent from '../ecs/components/StateComponent';
import { MetersPerPixel, physicsFilters } from '../constants';
import VisibleComponent from '../ecs/components/VisibleComponent';
import BulletComponent from '../ecs/components/tags/BulletComponent';

export default class EntityFactory {
    private entityManager: EntityManager;
    constructor(private app: Application, private physicsWorld: World) {}

    setEntityManager(entityManager: EntityManager) {
        this.entityManager = entityManager;
    }

    createPlayer(x: number, y: number): Entity {
        const size = 0.5;
        const halfSize = size / 2;
        const lineWidth = 2 * MetersPerPixel;
        const lineColor = 0xffffff;
        const speed = 15;

        const entity = this.entityManager.createEntity();
        const renderable = new Graphics();
        renderable.tint = 0xfff;
        renderable.filters = [];

        renderable
            .lineStyle(lineWidth, lineColor)
            .drawPolygon([new Point(-halfSize, -halfSize), new Point(halfSize, -halfSize), new Point(0, halfSize)])
            .lineStyle(0);

        const fixture = this.physicsWorld
            .createDynamicBody({
                position: new Vec2(x, y),
                userData: entity
            })
            .createFixture({
                shape: new Circle(size),
                filterCategoryBits: physicsFilters.PLAYER,
                filterMaskBits: physicsFilters.OBSTACLE
            });

        return entity
            .addComponent(BodyComponent, { value: fixture.getBody() })
            .addComponent(PositionComponent, { x, y })
            .addComponent(RenderableComponent, { value: renderable })
            .addComponent(RotationComponent)
            .addComponent(SpeedComponent, { value: speed })
            .addComponent(StateComponent, { shootCooldown: 100 })
            .addComponent(VelocityComponent)
            .addComponent(VisibleComponent);
    }

    createWall(x: number, y: number, width: number, height: number): Entity {
        const entity = this.entityManager.createEntity();
        const renderable = new Graphics().lineStyle(2 * MetersPerPixel, 0xffffff).drawRect(x, y, width, height);
        renderable.filters = [];

        const fixture = this.physicsWorld
            .createBody({
                position: new Vec2(x, y),
                userData: entity
            })
            .createFixture({
                shape: new Box(width * 0.5, height * 0.5, new Vec2(x + width * 0.5, y + height * 0.5)),
                filterCategoryBits: physicsFilters.OBSTACLE
            });

        return entity
            .addComponent(BodyComponent, { value: fixture.getBody() })
            .addComponent(PositionComponent, { x, y })
            .addComponent(RenderableComponent, { value: renderable })
            .addComponent(RotationComponent)
            .addComponent(VelocityComponent)
            .addComponent(VisibleComponent);
    }

    createBullet(x: number, y: number, angle: number): Entity {
        const size = 0.1;
        const speed = 20;

        const entity = this.entityManager.createEntity();
        const renderable = new Graphics()
            .beginFill(0xffffff)
            .drawCircle(x * MetersPerPixel, y * MetersPerPixel, size)
            .endFill();
        renderable.tint = 0x00ffff;
        renderable.filters = [];
        renderable.pivot.set(x * MetersPerPixel, y * MetersPerPixel);

        const fixture = this.physicsWorld
            .createDynamicBody({
                position: new Vec2(x, y),
                linearVelocity: new Vec2(speed * Math.cos(angle), speed * Math.sin(angle)),
                bullet: true,
                userData: entity
            })
            .createFixture({
                shape: new Circle(size),
                friction: -1,
                filterCategoryBits: physicsFilters.BULLET,
                filterMaskBits: physicsFilters.OBSTACLE
            });

        return entity
            .addComponent(BodyComponent, { value: fixture.getBody() })
            .addComponent(BulletComponent)
            .addComponent(PositionComponent, { x, y })
            .addComponent(RenderableComponent, { value: renderable })
            .addComponent(RotationComponent)
            .addComponent(VelocityComponent)
            .addComponent(VisibleComponent);
    }
}
