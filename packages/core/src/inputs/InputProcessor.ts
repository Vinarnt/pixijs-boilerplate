export default interface InputProcessor {

    /**
     *
     * @param key Tte key code pressed
     */
    onKeyDown(key: string): void;
    onKeyUp(key: string): void;

    /**
     *
     * @param x screen x position
     * @param y screen y position
     * @param pointer pointer index touching down
     * @param button the mouse button that triggered the event
     */
    onTouchDown(x: number, y: number, pointer: number, button: number):void;

    /**
     * Mouse or
     * @param x screen x position
     * @param y screen y position
     * @param pointer pointer index touching up
     * @param button the mouse button that triggered the event
     */
    onTouchUp(x: number, y: number, pointer: number, button: number): void;

    /**
     * Mouse movement
     *
     * @param x
     * @param y
     */
    onMouseMove(x: number, y: number): void;

    /**
     * Mouse wheel scrolling
     *
     * @param amount the amount of scrolling
     */
    onScroll(amount: number): void;
}
