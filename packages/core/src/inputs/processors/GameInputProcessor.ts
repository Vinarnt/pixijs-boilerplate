import { IPoint, Point } from 'pixi.js';
import { Viewport } from 'pixi-viewport';
import { Entity } from 'ecsy';

import InputAdapter from '../InputAdapter';
import StateComponent from '../../ecs/components/StateComponent';
import PositionComponent from '../../ecs/components/PositionComponent';
import EntityManager from '../../ecs/EntityManager';
import DebugSystem from '../../ecs/systems/DebugSystem';

export default class GameInputProcessor extends InputAdapter {
    private tmpPos: Point = new Point();

    constructor(private viewport: Viewport, private entityManager: EntityManager, private player: Entity) {
        super();
    }

    onKeyDown(key: string) {
        switch (key) {
            case 'KeyW':
                this.player.getMutableComponent(StateComponent).moveUp = true;
                break;
            case 'KeyS':
                this.player.getMutableComponent(StateComponent).moveDown = true;
                break;
            case 'KeyA':
                this.player.getMutableComponent(StateComponent).moveLeft = true;
                break;
            case 'KeyD':
                this.player.getMutableComponent(StateComponent).moveRight = true;
                break;
            case 'F2':
                const debugSystem = this.entityManager.world.getSystem(DebugSystem);
                debugSystem.enabled ? debugSystem.stop() : debugSystem.play();
                break;
            default:
                break;
        }
    }

    onKeyUp(key: string) {
        switch (key) {
            case 'KeyW':
                this.player.getMutableComponent(StateComponent).moveUp = false;
                break;
            case 'KeyS':
                this.player.getMutableComponent(StateComponent).moveDown = false;
                break;
            case 'KeyA':
                this.player.getMutableComponent(StateComponent).moveLeft = false;
                break;
            case 'KeyD':
                this.player.getMutableComponent(StateComponent).moveRight = false;
                break;
            default:
                break;
        }
    }

    onTouchDown(x: number, y: number, pointer: number, button: number) {
        if (button === 0) {
            // Left button
            this.player.getMutableComponent(StateComponent).shoot = true;
        }
    }

    onTouchUp(x: number, y: number, pointer: number, button: number) {
        if (button === 0) { // Left button
            this.player.getMutableComponent(StateComponent).shoot = false;
        }
    }

    onMouseMove(x: number, y: number) {
        const state: StateComponent = this.player.getMutableComponent(StateComponent);
        const position: PositionComponent = this.player.getComponent(PositionComponent);

        this.tmpPos.set(position.x, position.y);
        const localPosition: IPoint = this.viewport.toScreen(this.tmpPos);
        state.viewAngle = -Math.atan2(x - localPosition.x, y - localPosition.y);
    }

    onScroll(amount: number) {}
}
