import InputProcessor from './InputProcessor';

export default class InputAdapter implements InputProcessor {
    onKeyDown(key: string) {}

    onKeyUp(key: string) {}

    onMouseMove(x: number, y: number) {}

    onScroll(amount: number) {}

    onTouchDown(x: number, y: number, pointer: number, button: number) {}

    onTouchUp(x: number, y: number, pointer: number, button: number) {}
}
