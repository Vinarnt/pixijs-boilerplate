import { includes, intersection, pull } from 'lodash';
import InputProcessor from './InputProcessor';

class InputManager {
    private pressedKeys: Array<string>;
    private processors: Map<string, InputProcessor>;

    constructor() {
        this.pressedKeys = new Array<string>();
        this.processors = new Map<string, InputProcessor>();

        window.addEventListener('keydown', e => {
            const { code, repeat } = e;

            if (repeat) return;
            this.pressedKeys.push(code);
            this.processors.forEach(processor => processor.onKeyDown(code));
        });
        window.addEventListener('keyup', e => {
            const { code } = e;
            pull(this.pressedKeys, e.code);
            this.processors.forEach(processor => processor.onKeyUp(code));
        });
        window.addEventListener('pointerdown', e => {
            const { clientX, clientY, pointerId, button } = e;
            this.processors.forEach(processor => processor.onTouchDown(clientX, clientY, pointerId, button));
        });
        window.addEventListener('pointerup', e => {
            const { clientX, clientY, pointerId, button } = e;
            this.processors.forEach(processor => processor.onTouchUp(clientX, clientY, pointerId, button));
        });
        window.addEventListener('pointermove', e => {
            const { clientX, clientY } = e;
            this.processors.forEach(processor => processor.onMouseMove(clientX, clientY));
        });
        window.addEventListener('wheel', e => {
            const { deltaY } = e;
            this.processors.forEach(processor => processor.onScroll(deltaY));
        });
    }

    /**
     * Add an input processor
     *
     * @param name the name of the processor
     * @param processor the processor to map to the name
     */
    addProcessor(name: string, processor: InputProcessor) {
        if (processor) this.processors.set(name, processor);
    }

    /**
     * Remove the named input processor
     *
     * @param name the name of the input processor to remove
     */
    removeProcessor(name: string) {
        this.processors.delete(name);
    }

    /**
     * Get the named input processor or null if not found
     *
     * @param name the input processor name
     * @return  the mapped input processor
     */
    getProcessor(name: string): InputProcessor {
        return this.processors.get(name);
    }

    /**
     * Check if an key is pressed
     * See this list of {@link https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent|codes}
     *
     * @param key the key code to test
     * @return true if the key is pressed, false otherwise
     */
    isKeyPressed(key: string): boolean {
        return includes(this.pressedKeys, key);
    }

    /**
     * Check if some keys are pressed
     * See this list of {@link https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent|codes}
     *
     * @param keys the key codes to test
     * @return true if the keys are pressed, false otherwise
     */
    areKeysPressed(keys: Array<string>): boolean {
        return this.pressedKeys.length > 0 && intersection(this.pressedKeys, keys).length === keys.length;
    }
}

export default new InputManager();
