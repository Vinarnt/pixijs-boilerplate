export const PixelsPerMeter = 32;
export const MetersPerPixel = 1 / PixelsPerMeter;

export const WORLD_WIDTH = 100;
export const WORLD_HEIGHT = 100;

export const physicsFilters = {
    PLAYER: 1,
    BULLET: 2,
    OBSTACLE: 4
};
