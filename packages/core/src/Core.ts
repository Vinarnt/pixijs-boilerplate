import { Application } from 'pixi.js';
import * as FPS from 'yy-fps';
import { GameScene } from './scenes/GameScene';

export class Core {
    private app: Application;

    constructor() {
        this.app = new Application({
            antialias: true,
            transparent: false,
            resolution: window.devicePixelRatio || 1,
            sharedTicker: true,
            backgroundColor: 0x0,
            resizeTo: window
        });

        // FPS Graph
        const fps = new FPS({
            side: 'top-left',
            meterLineHeight: 1
        });
        this.app.ticker.add(() => fps.frame());

        document.body.appendChild(this.app.view);

        this.app.view.addEventListener('contextmenu', e => {
            e.preventDefault();
        });
        window.addEventListener('resize', () => {
            this.app.resize();
        });
        this.app.stage.addChild(new GameScene(this.app));
    }

    exit() {
        this.app.stop();
        this.app.destroy(true, { children: true, texture: true, baseTexture: true });
    }
}
