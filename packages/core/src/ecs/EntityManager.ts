import { Viewport } from 'pixi-viewport';
import { Entity, World as ECSWorld } from 'ecsy';
import { World as PhysicsWorld } from 'planck-js';

import MovementSystem from './systems/MovementSystem';
import RenderSystem from './systems/RenderSystem';
import PhysicsSystem from './systems/PhysicsSystem';
import StateSystem from './systems/StateSystem';
import DebugSystem from './systems/DebugSystem';
import EntityFactory from '../factories/EntityFactory';
import CollisionSystem from './systems/CollisionSystem';
import RemovableSystem from './systems/RemovableSystem';

export default class EntityManager {
    readonly world: ECSWorld;
    constructor(viewport: Viewport, physicsWorld: PhysicsWorld, entityFactory: EntityFactory) {
        entityFactory.setEntityManager(this);
        this.world = new ECSWorld()
            .registerSystem(RemovableSystem)
            // @ts-ignore
            .registerSystem(StateSystem, { entityFactory })
            // @ts-ignore
            .registerSystem(PhysicsSystem, { physicsWorld })
            .registerSystem(CollisionSystem)
            .registerSystem(MovementSystem)
            .registerSystem(DebugSystem, { viewport })
            .registerSystem(RenderSystem, { viewport });

        this.world.getSystem(DebugSystem).stop();
    }

    createEntity(): Entity {
        return this.world.createEntity();
    }

    update(delta: number, time: number) {
        this.world.execute(delta, time);
    }
}
