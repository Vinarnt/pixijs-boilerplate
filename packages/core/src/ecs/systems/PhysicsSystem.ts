import { Entity, System } from 'ecsy';
import { World as ECSWorld, World as PhysicsWorld } from 'planck-js';
import { pull } from 'lodash';

import BodyComponent from '../components/BodyComponent';
import PositionComponent from '../components/PositionComponent';
import RotationComponent from '../components/RotationComponent';
import CollisionComponent from '../components/CollisionComponent';

export default class PhysicsSystem extends System {
    static queries = {
        physicalEntities: {
            components: [BodyComponent, PositionComponent, RotationComponent],
            listen: {
                removed: true
            }
        }
    };

    private physicsWorld: PhysicsWorld;

    constructor(world: ECSWorld, attributes: { physicsWorld: PhysicsWorld }) {
        // @ts-ignore
        super(world, attributes);

        this.physicsWorld = attributes.physicsWorld;
        this.physicsWorld.on('begin-contact', ({ m_fixtureA: fixtureA, m_fixtureB: fixtureB }) => {
            const entityA = fixtureA.getBody().getUserData() as Entity;
            const entityB = fixtureB.getBody().getUserData() as Entity;
            if (!entityA.hasComponent(CollisionComponent)) entityA.addComponent(CollisionComponent);
            if (!entityB.hasComponent(CollisionComponent)) entityB.addComponent(CollisionComponent);

            const collisionComponentA: CollisionComponent = entityA.getMutableComponent(CollisionComponent);
            const collisionComponentB: CollisionComponent = entityB.getMutableComponent(CollisionComponent);

            collisionComponentA.collideWith.push(entityB);
            collisionComponentB.collideWith.push(entityA);
        });
        this.physicsWorld.on('end-contact', ({ m_fixtureA: fixtureA, m_fixtureB: fixtureB }) => {
            const entityA = fixtureA.getBody().getUserData() as Entity;
            const entityB = fixtureB.getBody().getUserData() as Entity;

            // @ts-ignore
            const collisionComponentA: CollisionComponent = entityA.getMutableComponent(CollisionComponent);
            const collisionComponentB: CollisionComponent = entityB.getMutableComponent(CollisionComponent);

            if (collisionComponentA) {
                if (collisionComponentA.collideWith.length <= 1) entityA.removeComponent(CollisionComponent);
                else pull(collisionComponentA.collideWith, entityB);
            }
            if (collisionComponentB) {
                if (collisionComponentB.collideWith.length <= 1) entityB.removeComponent(CollisionComponent);
                else pull(collisionComponentB.collideWith, entityA);
            }
        });
    }

    execute(delta: number, time: number) {
        this.physicsWorld.step(1 / 60, 6, 2);

        // @ts-ignore
        this.queries.physicalEntities.removed.forEach((entity: Entity) => {
            const body = entity.getRemovedComponent(BodyComponent).value;
            this.physicsWorld.destroyBody(body);
        });

        // @ts-ignore
        this.queries.physicalEntities.results.forEach(entity => {
            const body = entity.getComponent(BodyComponent).value;
            const position = entity.getComponent(PositionComponent);
            const rotation = entity.getComponent(RotationComponent);

            const { x: bodyX, y: bodyY } = body.getPosition();
            const bodyAngle = body.getAngle();

            if (bodyX !== position.x || bodyY !== position.y)
                entity.getMutableComponent(PositionComponent).set(bodyX, bodyY);
            if (bodyAngle !== rotation.value) entity.getMutableComponent(RotationComponent).value = bodyAngle;
        });
    }
}
