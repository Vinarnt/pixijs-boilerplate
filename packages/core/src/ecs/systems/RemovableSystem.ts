import { Entity, System } from 'ecsy';
import RemovableComponent from '../components/tags/RemovableComponent';

export default class RemovableSystem extends System {
    static queries = {
        removables: {
            components: [RemovableComponent],
            listen: {
                added: true
            }
        }
    };
    execute(delta: number, time: number) {
        // @ts-ignore
        this.queries.removables.added.forEach((entity: Entity) => {
            entity.remove();
        });
    }
}
