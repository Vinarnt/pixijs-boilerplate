import { Filter, Graphics } from 'pixi.js';
import { Entity, System } from 'ecsy';
import { World } from 'planck-js';
import { findIndex, pull } from 'lodash';
import { ColorReplaceFilter } from '@pixi/filter-color-replace';

import CollisionComponent from '../components/CollisionComponent';
import RenderableComponent from '../components/RenderableComponent';
import BulletComponent from '../components/tags/BulletComponent';
import RemovableComponent from '../components/tags/RemovableComponent';

export default class CollisionSystem extends System {
    static queries = {
        collisions: {
            components: [CollisionComponent],
            listen: {
                added: true,
                removed: true,
                changed: true
            }
        }
    };

    constructor(world: World, attributes: {}) {
        // @ts-ignore
        super(world, attributes);
    }

    execute(delta: number, time: number) {
        // First collision started
        // @ts-ignore
        this.queries.collisions.added.forEach((entity: Entity) => {
            const renderable = entity.getComponent(RenderableComponent).value;
            if (renderable instanceof Graphics) {
                let colorFilter: ColorReplaceFilter = renderable.filters.find(
                    filter => filter instanceof ColorReplaceFilter
                ) as ColorReplaceFilter;
                if (!colorFilter) {
                    colorFilter = new ColorReplaceFilter(renderable.tint);
                    renderable.filters.push(colorFilter);
                }
                colorFilter.newColor = Math.floor(Math.random() * 16777215);
            }

            if (entity.hasComponent(BulletComponent)) {
                // Defer entity remove to next frame because the physics system in charge of removing bodies has already been execute
                entity.addComponent(RemovableComponent);
            }
        });

        // Last collision ended
        // @ts-ignore
        this.queries.collisions.removed.forEach((entity: Entity) => {
            // @ts-ignore
            const renderable = entity.getComponent(RenderableComponent, true).value;
            // @ts-ignore
            const collideWith = entity.getComponent(CollisionComponent, true).collideWith;
            const handleCollisionEnd = () => {
                let colorFilter: ColorReplaceFilter = renderable.filters.find(
                    (filter: Filter) => filter instanceof ColorReplaceFilter
                ) as ColorReplaceFilter;
                if (colorFilter) {
                    pull(renderable.filters, colorFilter);
                }
            };

            // @ts-ignore
            if (findIndex(collideWith, (entity: Entity) => entity.hasComponent(BulletComponent, true)) > -1)
                setTimeout(handleCollisionEnd, 250);
            else handleCollisionEnd();
        });

        // Collisions added or removed
        // @ts-ignore
        this.queries.collisions.changed.forEach((entity: Entity) => {
            //console.log('Changed collision component for entity ', entity.id);
        });
    }
}
