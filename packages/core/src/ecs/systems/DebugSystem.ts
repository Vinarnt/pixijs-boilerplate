import { Graphics } from 'pixi.js';
import { Viewport } from 'pixi-viewport';
import { CircleShape, PolygonShape } from 'planck-js';
import { Entity, System, World } from 'ecsy';

import BodyComponent from '../components/BodyComponent';
import { MetersPerPixel } from '../../constants';

export default class DebugSystem extends System {
    static queries = {
        physicalEntities: {
            components: [BodyComponent]
        }
    };

    private viewport: Viewport;
    private readonly debugLayer: Graphics;

    constructor(private world: World, attributes: { viewport: Viewport }) {
        // @ts-ignore
        super(world, attributes);

        this.viewport = attributes.viewport;
        this.debugLayer = new Graphics();
        this.debugLayer.zIndex = 100;
        this.viewport.addChild(this.debugLayer);
    }

    stop() {
        super.stop();

        this.setDebugVisible(false);
    }

    play() {
        super.play();

        this.setDebugVisible(true);
    }

    private setDebugVisible(visible: boolean) {
        this.debugLayer.visible = visible;
    }

    private drawPhysicsDebug(entities: Array<Entity>) {
        this.debugLayer.clear();

        entities.forEach((entity: Entity) => {
            const body = entity.getComponent(BodyComponent).value;
            const bodyPosition = body.getPosition();

            for (let fixture = body.getFixtureList(); fixture; fixture = fixture.getNext()) {
                switch (fixture.getType()) {
                    case 'polygon':
                        const vertices = (fixture.m_shape as PolygonShape).m_vertices;
                        const path = vertices.flatMap(current => [
                            bodyPosition.x + current.x,
                            bodyPosition.y + current.y
                        ]);
                        this.debugLayer.lineStyle(MetersPerPixel, 0x0000ff).drawPolygon(path);
                        break;
                    case 'circle':
                        const shape = fixture.m_shape as CircleShape;
                        const radius = shape.m_radius;
                        const fixturePosition = shape.getCenter();
                        this.debugLayer
                            .lineStyle(MetersPerPixel, 0x0000ff)
                            .drawCircle(bodyPosition.x + fixturePosition.x, bodyPosition.y + fixturePosition.y, radius);
                        break;
                    default:
                }
            }
        });
    }

    execute(delta: number, time: number) {
        // @ts-ignore
        this.drawPhysicsDebug(this.queries.physicalEntities.results);
    }
}
