import {Entity, Not, System} from 'ecsy';
import VelocityComponent from '../components/VelocityComponent';
import PositionComponent from '../components/PositionComponent';
import SpeedComponent from '../components/SpeedComponent';
import BodyComponent from '../components/BodyComponent';

export default class MovementSystem extends System {
    static queries = {
        movement: {
            components: [Not(BodyComponent), VelocityComponent, PositionComponent]
        }
    };
    execute(delta: number, time: number) {
        // @ts-ignore
        this.queries.movement.results.forEach((entity: Entity) => {
            const velocity = entity.getComponent(VelocityComponent);

            if (velocity.x || velocity.y) {
                const position = entity.getMutableComponent(PositionComponent);
                const speed = entity.getComponent(SpeedComponent).value;

                if (velocity.x) position.x += velocity.x * speed * delta;
                if (velocity.y) position.y += velocity.y * speed * delta;
            }
        });
    }
}
