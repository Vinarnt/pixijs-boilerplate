import { DEG_TO_RAD } from 'pixi.js';
import { Entity, System, World } from 'ecsy';
import { Body, Vec2 } from 'planck-js';
import sound from 'pixi-sound';

import BodyComponent from '../components/BodyComponent';
import StateComponent from '../components/StateComponent';
import SpeedComponent from '../components/SpeedComponent';
import EntityFactory from '../../factories/EntityFactory';
import PositionComponent from '../components/PositionComponent';
import RotationComponent from '../components/RotationComponent';

export default class StateSystem extends System {
    static queries = {
        stateEntities: {
            components: [StateComponent]
        }
    };

    private entityFactory: EntityFactory;

    constructor(world: World, attributes: { entityFactory: EntityFactory }) {
        // @ts-ignore
        super(world, attributes);

        this.entityFactory = attributes.entityFactory;
    }

    execute(delta: number, time: number) {
        // @ts-ignore
        this.queries.stateEntities.results.forEach((entity: Entity) => {
            const state: StateComponent = entity.getComponent(StateComponent);
            const body: Body = entity.getComponent(BodyComponent).value;
            const position = entity.getComponent(PositionComponent);
            const rotation = entity.getComponent(RotationComponent).value;
            const speed: number = entity.getComponent(SpeedComponent).value;
            const { moveUp, moveDown, moveLeft, moveRight, shoot, lastShot, shootCooldown, viewAngle } = state;

            const newVelocity = new Vec2();

            // Vertical movement
            if (moveDown && moveUp) {
                newVelocity.y = 0;
            } else if (moveUp) {
                newVelocity.y = -speed;
            } else if (moveDown) {
                newVelocity.y = speed;
            }

            // Horizontal movement
            if (moveLeft && moveRight) {
                newVelocity.x = 0;
            } else if (moveLeft) {
                newVelocity.x = -speed;
            } else if (moveRight) {
                newVelocity.x = speed;
            }

            // Shoot
            if (shoot) {
                if (time - lastShot >= shootCooldown) {
                    this.entityFactory.createBullet(position.x, position.y, rotation + 90 * DEG_TO_RAD);
                    sound.play('shot');
                    state.lastShot = time;
                }
            }

            body.setLinearVelocity(newVelocity);
            body.setAngle(viewAngle);
        });
    }
}
