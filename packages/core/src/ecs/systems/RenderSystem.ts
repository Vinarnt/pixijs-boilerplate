import { DisplayObject } from 'pixi.js';
import { Entity, System, World } from 'ecsy';

import RenderableComponent from '../components/RenderableComponent';
import PositionComponent from '../components/PositionComponent';
import RotationComponent from '../components/RotationComponent';
import VisibleComponent from '../components/VisibleComponent';

export default class RenderSystem extends System {
    static queries = {
        renderables: {
            components: [RenderableComponent, VisibleComponent],
            listen: {
                added: true,
                removed: true
            }
        },
        positionChange: {
            components: [RenderableComponent, PositionComponent],
            listen: {
                changed: [PositionComponent]
            }
        },
        rotationChange: {
            components: [RenderableComponent, RotationComponent],
            listen: {
                changed: [RotationComponent]
            }
        },
        visibilityChange: {
            components: [RenderableComponent, VisibleComponent],
            listen: {
                changed: [VisibleComponent]
            }
        }
    };

    constructor(world: World, private attributes: object) {
        // @ts-ignore
        super(world, attributes);
    }

    execute(delta: number, time: number) {
        // @ts-ignore
        this.queries.renderables.added.forEach((entity: Entity) => {
            const renderable = entity.getComponent(RenderableComponent).value;
            if (renderable) {
                RenderSystem.updatePosition(entity);
                RenderSystem.updateRotation(entity);
                // @ts-ignore
                this.attributes.viewport.addChild(renderable);
            }
        });
        // @ts-ignore
        this.queries.renderables.removed.forEach((entity: Entity) => {
            const renderable = entity.getRemovedComponent(RenderableComponent).value;
            // @ts-ignore
            this.attributes.viewport.removeChild(renderable);
        });
        // @ts-ignore
        this.queries.positionChange.changed.forEach((entity: Entity) => {
            // @ts-ignore
            if (entity.alive) RenderSystem.updatePosition(entity);
        });
        // @ts-ignore
        this.queries.rotationChange.changed.forEach((entity: Entity) => {
            // @ts-ignore
            if (entity.alive) RenderSystem.updateRotation(entity);
        });
        // @ts-ignore
        this.queries.visibilityChange.changed.forEach((entity: Entity) => {
            const visible = entity.getComponent(VisibleComponent).value;
            const renderable = entity.getComponent(RenderableComponent).value;
            renderable.visible = visible;
        });
    }

    private static updatePosition(entity: Entity) {
        const renderable: DisplayObject = entity.getComponent(RenderableComponent).value;
        const position = entity.getComponent(PositionComponent);

        renderable.position.set(position.x, position.y);
    }

    private static updateRotation(entity: Entity) {
        const renderable: DisplayObject = entity.getComponent(RenderableComponent).value;
        renderable.rotation = entity.getComponent(RotationComponent).value;
    }
}
