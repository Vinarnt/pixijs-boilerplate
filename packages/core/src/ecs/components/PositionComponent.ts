import { Component } from 'ecsy';

export default class PositionComponent extends Component {
    x: number = 0;
    y: number = 0;

    set(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    reset() {
        this.x = 0;
        this.y = 0;
    }
}
