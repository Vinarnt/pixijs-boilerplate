import { Component } from 'ecsy';

export default class SpeedComponent extends Component {
    value: number = 10;

    reset() {
        this.value = 0;
    }
}
