import { Component } from 'ecsy';

export default class RotationComponent extends Component {
    value: number = 0;

    reset() {
        this.value = 0;
    }
}
