import { Component } from 'ecsy';

export default class StateComponent extends Component {
    moveUp: boolean = false;
    moveDown: boolean = false;
    moveLeft: boolean = false;
    moveRight: boolean = false;
    shoot: boolean = false;
    viewAngle: number = 0;
    lastShot: number = 0;

    // In ms
    shootCooldown: number = 500;

    reset() {
        this.moveUp = false;
        this.moveDown = false;
        this.moveLeft = false;
        this.moveRight = false;
        this.shoot = false;
        this.viewAngle = 0;
        this.lastShot = 0;
        this.shootCooldown = 500;
    }
}
