import { Component, Entity } from 'ecsy';

export default class CollisionComponent extends Component {
    collideWith: Array<Entity> = new Array<Entity>();

    reset() {
        this.collideWith = new Array<Entity>();
    }
}
