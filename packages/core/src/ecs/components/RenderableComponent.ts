import { DisplayObject } from 'pixi.js';
import { Component } from 'ecsy';

export default class RenderableComponent extends Component {
    value: DisplayObject;

    reset() {
        this.value = undefined;
    }
}
