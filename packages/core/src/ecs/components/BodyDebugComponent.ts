import { Component, Entity } from 'ecsy';

export default class BodyDebugComponent extends Component {
    debugShape: Entity;

    reset() {
        this.debugShape = undefined;
    }
}
