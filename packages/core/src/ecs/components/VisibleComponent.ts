import { Component } from 'ecsy';

export default class VisibleComponent extends Component {
    value: boolean = true;

    reset() {
        this.value = true;
    }
}
