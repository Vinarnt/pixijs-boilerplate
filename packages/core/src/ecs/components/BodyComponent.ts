import { Component } from 'ecsy';
import { Body } from 'planck-js';

export default class BodyComponent extends Component {
    value: Body;

    reset() {
        this.value = undefined;
    }
}
