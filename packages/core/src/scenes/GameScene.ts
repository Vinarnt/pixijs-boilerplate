import { Application, Container, Ticker } from 'pixi.js';
import { Viewport } from 'pixi-viewport';
import sound from 'pixi-sound';
import { Entity } from 'ecsy';
import { World } from 'planck-js';

import EntityFactory from '../factories/EntityFactory';
import InputManager from '../inputs/InputManager';
import GameInputProcessor from '../inputs/processors/GameInputProcessor';
import EntityManager from '../ecs/EntityManager';
import RenderableComponent from '../ecs/components/RenderableComponent';
import { MetersPerPixel, PixelsPerMeter, WORLD_HEIGHT, WORLD_WIDTH } from '../constants';

export class GameScene extends Container {
    readonly viewport: Viewport;
    readonly entityFactory: EntityFactory;
    readonly entityManager: EntityManager;
    readonly physicsWorld: World;

    constructor(public app: Application) {
        super();

        // Load assets
        sound.add('shot', {
            url: 'assets/shot.wav',
            preload: true,
            volume: 0.2
        });

        // Add ecs update to shared ticker before the viewport update to avoid render logic desync
        Ticker.shared.add(delta => {
            this.entityManager.update(delta, performance.now());
        });
        this.viewport = new Viewport({
            screenWidth: window.innerWidth,
            screenHeight: window.innerHeight,
            worldWidth: WORLD_WIDTH,
            worldHeight: WORLD_HEIGHT,
            interaction: this.app.renderer.plugins.interaction
        });
        this.viewport.sortableChildren = true;

        this.physicsWorld = new World();
        this.entityFactory = new EntityFactory(this.app, this.physicsWorld);
        this.entityManager = new EntityManager(this.viewport, this.physicsWorld, this.entityFactory);

        for (let i = 0; i < 1000; i++) {
            this.entityFactory.createWall(
                Math.floor(Math.random() * WORLD_WIDTH),
                Math.floor(Math.random() * WORLD_HEIGHT),
                1 + Math.floor(Math.random() * 5),
                1 + Math.floor(Math.random() * 5)
            );
        }

        const player: Entity = this.entityFactory.createPlayer(
            (this.app.screen.width * MetersPerPixel) / 2,
            (this.app.screen.height * MetersPerPixel) / 2
        );
        const playerRenderable = player.getComponent(RenderableComponent).value;

        this.viewport
            .follow(playerRenderable)
            .setZoom(PixelsPerMeter)
            .zoomPercent(0.45);

        window.addEventListener('resize', () => {
            this.viewport.resize(window.innerWidth, window.innerHeight, WORLD_WIDTH, WORLD_HEIGHT);
        });
        this.addChild(this.viewport);

        InputManager.addProcessor('gameInput', new GameInputProcessor(this.viewport, this.entityManager, player));
    }
}
