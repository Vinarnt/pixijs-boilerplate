const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const rules = require('./webpack.rules');

module.exports = {
    module: {
        rules
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    // eslint-disable-next-line @typescript-eslint/camelcase
                    keep_classnames: /\w*(System|Component)/,
                    output: {
                        comments: false
                    }
                },
                extractComments: false
            })
        ]
    },
    plugins: [
        new ForkTsCheckerWebpackPlugin({
            async: false
        }),
        new CopyWebpackPlugin([{ from: '../core/assets/**/*', to: 'main_window/assets/', flatten: true }])
    ],
    resolve: {
        extensions: ['.js', '.ts', '.tsx']
    }
};
