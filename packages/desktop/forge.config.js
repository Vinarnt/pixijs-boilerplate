module.exports = {
    makers: [
        {
            name: '@electron-forge/maker-squirrel',
            config: {
                name: 'MyPixiJSGame'
            }
        },
        {
            name: '@electron-forge/maker-zip'
        },
        {
            name: '@electron-forge/maker-deb',
            config: {
                options: {
                    productName: 'My pixijs game',
                    productDescription: 'A simple game made with pixijs',
                    description: 'A simple game made with pixijs',
                    categories: ['Game']
                }
            }
        },
        {
            name: '@electron-forge/maker-rpm',
            config: {
                options: {
                    productName: 'My pixijs game',
                    productDescription: 'A simple game made with pixijs',
                    description: 'A simple game made with pixijs',
                    categories: ['Game']
                }
            }
        }
    ],
    plugins: [
        [
            '@electron-forge/plugin-webpack',
            {
                mainConfig: './webpack.main.config.js',
                renderer: {
                    config: './webpack.renderer.config.js',
                    entryPoints: [
                        {
                            html: './src/index.html',
                            js: './src/renderer.ts',
                            name: 'main_window'
                        }
                    ]
                }
            }
        ]
    ]
};
