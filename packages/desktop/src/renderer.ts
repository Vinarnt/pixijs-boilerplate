import { Core } from 'core';

let core = new Core();

if (module.hot) {
    module.hot.accept('core', function() {
        window.location.reload();
    });
}
