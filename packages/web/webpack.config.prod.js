const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const path = require('path');

module.exports = {
    mode: 'production',
    entry: {
        core: path.resolve(__dirname, '../core/src/index.ts'),
        web: path.resolve(__dirname, 'src/index.ts')
    },
    resolve: {
        extensions: ['.js', '.ts', '.tsx']
    },
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                terserOptions: {
                    // eslint-disable-next-line @typescript-eslint/camelcase
                    keep_classnames: /\w*(System|Component)/,
                    output: {
                        comments: false
                    }
                },
                extractComments: false
            })
        ]
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader'
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            {
                from: '../core//assets',
                to: 'assets'
            }
        ]),
        new HTMLWebpackPlugin({
            template: 'index.html',
            hash: true,
            minify: true
        })
    ]
};
